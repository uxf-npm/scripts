const GitLab = require("../GitLab");
const Slack = require("../Slack");
const GoogleChat = require("../GoogleChat");

function addCommitsToIssues(commits, issues) {
    issues.forEach(issue => {
        if (issue.commits === undefined) {
            issue.commits = [];
        }
    });

    commits.forEach(commit => {
        if (commit.issueIds.length > 0) {
            issues.forEach(issue => {
                if (commit.issueIds.includes(issue.iid)) {
                    issue.commits.push(commit);
                }
            });
        }
    });
}

function getIssueIdsFromCommits(commits) {
    const ids = [];
    commits.forEach(c => ids.push(...c.issueIds));
    return ids.filter((v, i, s) => s.indexOf(v) === i);
}

function generateSlackMessage(issues, commitsWithoutIssues, messageTitle) {
    let message = messageTitle;

    issues.forEach(issue => {
        message += `\n\n*${issue.title}* <${issue.web_url}|#${issue.iid}>\n${issue.commits
            .map(generateSlackCommitMessage)
            .join("\n")}`;
    });

    if (commitsWithoutIssues.length > 0) {
        message += `\n\n*Commity bez issue*\n${commitsWithoutIssues.map(generateSlackCommitMessage).join("\n")}`;
    }

    return message;
}

function generateGoogleMessage(issues, commitsWithoutIssues, messageTitle) {
    const texts = [messageTitle];
    issues.forEach(issue => {
        texts.push(`*${issue.title}* <${issue.web_url}|#${issue.iid}>`);
        issue.commits.forEach((commit) => texts.push(generateSlackCommitMessage(commit)));
    });
    if (commitsWithoutIssues.length > 0) {
        texts.push(`*Commity bez issue*`);
        commitsWithoutIssues.forEach((commit) => texts.push(generateSlackCommitMessage(commit)));
    }
    return {
        text: texts.join('\n')
    };
}

function generateSlackCommitMessage(commit) {
    const { title, parsedTitle, short_id, web_url, author_email } = commit;

    return parsedTitle.type
        ? `• _${parsedTitle.type.toUpperCase()}_ ${parsedTitle.scope} - ${
              parsedTitle.subject
          } <${web_url}|${short_id}> (${author_email})`
        : `• ${title} <${web_url}|${short_id}> (${author_email})`;
}

function generateCommitMessage(commit) {
    const { title, parsedTitle, short_id, web_url, author_email } = commit;

    return parsedTitle.type
        ? `- **${parsedTitle.type.toUpperCase()}** ${parsedTitle.subject} [${short_id}](${web_url}) (${author_email})`
        : `- ${title} [${short_id}](${web_url}) (${author_email})`;
}

module.exports = async (dryRun, channel, messageTitle) => {
    const lastTag = await GitLab.getLastTag();
    const commits = await GitLab.loadCommits(lastTag ? lastTag.commit.committed_date : null);
    const issues = await GitLab.loadIssues(getIssueIdsFromCommits(commits));

    addCommitsToIssues(commits, issues);

    const commitsWithoutIssue = commits.filter(c => c.issueIds.length === 0);

    await Slack.chatPostMessage(
        channel,
        { text: generateSlackMessage(issues, commitsWithoutIssue, messageTitle) },
        dryRun,
    );

    await GoogleChat.chatPostMessage(
        generateGoogleMessage(issues, commitsWithoutIssue, messageTitle),
        dryRun,
    );

    await GitLab.createRelease(commits.map(generateCommitMessage).join("\n"), dryRun);
};
