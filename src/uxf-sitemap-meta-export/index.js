const Sitemap = require("../Sitemap");
const cheerio = require("cheerio");
const fs = require('fs');

module.exports = async function run() {
    if (!process.env.SITEMAP_URL) {
        // eslint-disable-next-line no-console
        console.error("Environment variable SITEMAP_URL is empty.");
        return process.exit(1);
    }

    const urls = await Sitemap.getSitemap(process.env.SITEMAP_URL);

    let i = 0;
    const lines = [['url', 'title', 'og:title', 'description', 'og:description'].join(';')];

    for (const url of urls) {
        process.stdout.write(`${++i} / ${urls.length} ${url} \n`);
        try {
            const response = await Sitemap.axios.get(url);

            const $ = cheerio.load(response.data, { xmlMode: true, decodeEntities: false });

            let title = '';
            let ogTitle = '';
            let description = '';
            let ogDescription = '';
            $('meta[property="og:title"]').each(function () {

                ogTitle=$(this).attr('content');
            });
            $('meta[property="og:description"]').each(function () {
                ogDescription=$(this).attr('content');
            });
            $('meta[name="description"]').each(function () {
                description=$(this).attr('content');
            });
            $('head title').each(function () {
                title=$(this).html();
            });

            lines.push([url, title, ogTitle, description, ogDescription].join(';'));
        } catch (e) {
            console.log(e);
        }
    }

    try {
        fs.writeFileSync('export.csv', lines.join('\n'));
    } catch (e) {
        console.log('ERROR', e);
    }

    process.exit(0);
};
