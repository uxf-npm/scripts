const { argv, env } = require("process");

module.exports = async () => {
    const cli = require("yargs")
        .command("$0", "UXF sitemap meta exporter", yargs => {
            yargs.demandCommand(0, 0).usage(`UXF sitemap meta exporter
Usage:
  sitemap-meta-export [options]
  
Environment variables:
  SITEMAP_URL       - required
  HTTP_USERNAME     - optional
  HTTP_PASSWORD     - optional
  SLACK_WEBHOOK_URL - optional`);
        })
        .option("url", {
            describe: "Sitemap url (or use environment variable SITEMAP_URL)",
            type: "string",
            group: "Options",
        })
        .option("u", {
            alias: "http-username",
            describe: "Http authorization username (or use environment variable HTTP_USERNAME)",
            type: "string",
            group: "Options",
        })
        .option("p", {
            alias: "http-password",
            describe: "Http authorization password (or use environment variable HTTP_PASSWORD)",
            type: "string",
            group: "Options",
        })
        .option("d", {
            alias: "dry-run",
            describe: "Skip slack notification",
            type: "boolean",
            group: "Options",
        })
        .option("h", { alias: "help", group: "Options" })
        .strict(false)
        .exitProcess(false);

    try {
        const { help, url, d: dryRun, ...options } = cli.parse(argv.slice(2));

        if (Boolean(help)) {
            return 0;
        }

        if (options["http-username"] && options["http-password"]) {
            env.HTTP_USERNAME = options["http-username"];
            env.HTTP_PASSWORD = options["http-password"];
        }

        if (url) {
            env.SITEMAP_URL = url;
        }

        await require("./index")(dryRun);
    } catch (e) {
        console.error(e);
        return 1;
    }
};
