function debug(message) {
  console.debug(message);
}

function info(message) {
  console.info(message);
}

module.exports = {
  debug,
  info,
};
