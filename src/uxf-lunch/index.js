const axios = require("axios");
const Slack = require("../Slack");

const numbers = [":keycap_star:", ":one:", ":two:", ":three:", ":four:", ":five:", ":six:", ":seven:"];
const paymentsUrl = "https://gitlab.uxf.cz/Vejvoda/obedy/-/wikis/%C4%8C%C3%ADsla-%C3%BA%C4%8Dt%C5%AF";

module.exports = async slackChannel => {
    const { data } = await axios.get("https://hotel.servispc-liberec.cz/server/api/data-jidelak");

    const menu = data.ListekPolozky.filter(({ druh }) => druh === "1").map(v => ({
        ...v,
        cena: Number.parseInt(v.cena),
    }));

    const blocks = [
        {
            type: "section",
            text: {
                type: "mrkdwn",
                text: "*Hotel Radnice*",
            },
        },
    ];

    menu.forEach((menuItem, index) => {
        const { polozka, cena } = menuItem;
        blocks.push({
            type: "section",
            text: {
                type: "mrkdwn",
                text: `${numbers[index]}  ${polozka} - \`${cena}Kč\``,
            },
        });
    });
    blocks.push({
        type: "context",
        elements: [
            {
                type: "mrkdwn",
                text: `:eyes: Ceny neobsahují *10Kč* za krabičku (máme vlastní krabičky)\n:moneybag: QR kódy a čísla účtů <${paymentsUrl}|zde> - po zaplacení přidat :white_check_mark:`,
            },
            {
                type: "mrkdwn",
                text:
                    ":bulb: Objednávky ideálně formou přidání reakce :keycap_star:, :one:, :two:, ... Ať objednávající nemusí sčítat :-)",
            },
        ],
    });

    await Slack.chatPostMessage(slackChannel, { blocks });
};
