const { argv, env } = require("process");

module.exports = async () => {
    const cli = require("yargs")
        .command("$0", "UXF release helper", yargs => {
            yargs.demandCommand(0, 0).usage(`UXF lunch
Usage:
  uxf-lunch [options]
  
Environment variables:
  SLACK_TOKEN - required`);
        })
        .option("slack-channel", {
            describe: "Slack channel",
            type: "string",
            group: "Options",
        })
        .option("h", { alias: "help", group: "Options" })
        .strict(false)
        .exitProcess(false);

    try {
        const { help, ...options } = cli.parse(argv.slice(2));

        if (Boolean(help)) {
            return 0;
        }

        if (!env.SLACK_TOKEN) {
            console.log("Slack token must be set. Use environment variable SLACK_TOKEN.");
            return 1;
        }

        if (!options["slack-channel"]) {
            console.log("Slack channel must be set. Use parameter --slack-channel");
            return 1;
        }

        await require("./index")(options["slack-channel"]);
    } catch (e) {
        console.error(e);
        return 1;
    }
};
