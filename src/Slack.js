const { env } = require("process");
const { create } = require("axios");

const axios = create({
    baseURL: "https://slack.com/api",
    headers: {
        Authorization: `Bearer ${env.SLACK_TOKEN}`,
    },
});

/**
 * @see https://api.slack.com/methods/chat.postMessage
 * @returns {Promise<void>}
 */
async function chatPostMessage(channel, data, dryRun = false) {
    if (env.SLACK_TOKEN && !dryRun) {
        const res = await axios.post("/chat.postMessage", { ...data, channel });
        if (res.data.ok === false) {
            process.stdout.write("SLACK: chat.postMessage error - " + JSON.stringify(res.data));
            return;
        }
        process.stdout.write("SLACK: chat.postMessage - done\n");
        return;
    }
    process.stdout.write("SLACK: chat.postMessage - skipped\n");
}

module.exports = {
    chatPostMessage,
};
