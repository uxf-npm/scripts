const { env } = require("process");
const { create } = require("axios");

const axios = create({});

/**
 * @see https://developers.google.com/chat/api/reference/rest/v1/cards-v1
 * @returns {Promise<void>}
 */
async function chatPostMessage(data, dryRun) {
    if (env.GOOGLE_WEBHOOK_URL && !dryRun) {
        await axios.post(env.GOOGLE_WEBHOOK_URL, { ...data });
    } else {
        process.stdout.write("GOOGLE CHAT: chat.postMessage - skipped");
    }
}

module.exports = {
    chatPostMessage,
};
