const https = require('https');
const { create } = require("axios");
const cheerio = require("cheerio");

const { HTTP_USERNAME, HTTP_PASSWORD } = process.env;

async function getSitemap(xml) {
    const { data } = await axios.get(xml);
    const $ = cheerio.load(data, { xmlMode: true });

    const urls = [];

    $("loc").each(function () {
        urls.push($(this).text());
    });

    return urls;
}

const agent = new https.Agent({
    rejectUnauthorized: false
});

const axios = create({
    auth:
        HTTP_PASSWORD && HTTP_USERNAME
            ? {
                  username: HTTP_USERNAME,
                  password: HTTP_PASSWORD,
              }
            : undefined,
    withCredentials: true,
    maxRedirects: 0,
    timeout: 20000,
    httpsAgent: agent,
    headers: {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate, br',
        'Accept-Language': 'en-US,en;q=0.9,cs-CZ;q=0.8,cs;q=0.7,de;q=0.6',
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive',
        'Pragma': 'no-cache',
        'Sec-Ch-Ua': '"Google Chrome";v="111", "Not(A:Brand";v="8", "Chromium";v="111"',
        'Sec-Ch-Ua-Arch': '"x86"',
        'Sec-Ch-Ua-Mobile': '?0',
        'Sec-Ch-Ua-Platform': '"Windows"',
        'Sec-Fetch-Dest': 'document',
        'Sec-Fetch-Mode': 'navigate',
        'Sec-Fetch-Site': 'cross-site',
        'Sec-Fetch-User': '?1',
        'Sec-Fetch-User-Agent': '?1',
    },
});

module.exports = {
    getSitemap,
    axios,
};
