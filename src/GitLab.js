const { env } = require("process");
const { create } = require("axios");
const moment = require("moment");
const parse = require("@commitlint/parse").default;

const axios = create({
    baseURL: `${env.CI_SERVER_URL}/api/v4`,

    headers: {
        Authorization: `Bearer ${env.GITLAB_TOKEN}`,
    },
});

async function loadCommits(from) {
    const commits = [];
    console.log(`- start date: ${from}`);

    let nextPage = "1";
    do {
        const response = await axios.get(`/projects/${env.CI_PROJECT_ID}/repository/commits`, {
            params: {
                ref_name: "master",
                since: from ? moment(from).format() : undefined,
                per_page: "100",
                page: nextPage,
            },
        });
        nextPage = response.headers["x-next-page"];
        commits.push(...response.data);
    } while (nextPage);

    const preparedCommits = [];
    for (const commit of commits) {
        if (commit.created_at === from) {
            continue;
        }

        const parsedTitle = await parse(commit.title, undefined, { issuePrefixes: ["#"] });

        preparedCommits.push({
            ...commit,
            parsedTitle: {
                scope: parsedTitle.scope,
                type: parsedTitle.type,
                subject: (parsedTitle.subject || commit.title).replace(/(#[0-9]*)/g, "").trim(),
            },
            issueIds: parsedTitle.references.map(ref => Number.parseInt(ref.issue)).filter(i => !!i),
        });
    }

    return preparedCommits;
}

async function loadIssues(iids) {
    if (iids.length === 0) {
        return [];
    }

    const { data } = await axios.get(`/projects/${env.CI_PROJECT_ID}/issues`, {
        params: { iids },
    });

    return data;
}

async function getLastTag() {
    const response = await axios.get(`/projects/${env.CI_PROJECT_ID}/repository/tags`, {
        params: {
            search: "^release-",
        },
    });

    const tags = response.data;

    if (tags.length === 0) {
        console.log("- release tag not found");
        return null;
    }

    console.log(`- last tag: ${tags[0].name}`);
    return tags[0];
}

async function createRelease(description, dryRun = false) {
    const tag = "release-" + moment().format("YYYY-MM-DD-HH-mm");

    if (dryRun) {
        console.log(`\n🎉🎉🎉 Release "${tag}" published (skipped in dry run)\n\n${description}`);
        return;
    }

    await axios.post(
        `/projects/${env.CI_PROJECT_ID}/releases`,
        { description, tag_name: tag, ref: 'master' }
    );

    console.log(`\n🎉🎉🎉 Release "${tag}" published\n\n${description}`);
}

module.exports = {
    loadCommits,
    loadIssues,
    getLastTag,
    createRelease,
};
