const Slack = require("../Slack");
const Sitemap = require("../Sitemap");
const { performance } = require("perf_hooks");
const { env, stdout } = require("process");
const { axios } = require("../Sitemap");
const cheerio = require("cheerio");
const GoogleChat = require("../GoogleChat");

/**
 *
 * @typedef {{parentUrl: (string | undefined), isImg: boolean, time: number, ttl: number, url: string, status: number, message: (string | undefined), shouldIgnoreError: (boolean | undefined)}} UrlCheckResponse
 */

const MAX_TTL = 3;
const TESTED_URLS = [];
const IMAGES_LABEL = "🏞 Images:";
const URLS_LABEL = "🔗 Links:";

/**
 *
 * @param length {number}
 * @return {string}
 */
function createTabSpace(length = 1) {
    return "".padStart(2 * length);
}

/**
 *
 * @param url {string}
 * @return {boolean}
 */
function isImageUrl(url) {
    return new RegExp("[a-z].*(\\.jpg|\\.png|\\.webp|\\.avif|\\.gif)$", "gim").test(url);
}

/**
 *
 * @param url {string}
 * @param status {number}
 * @param e {Error}
 * @returns {boolean}
 */
function shouldIgnoreError(url, status, e) {
    if (status === 999 && url.startsWith("https://www.linkedin.com")) {
        return true;
    }
    if ((status === -1 || status === 302) && url.startsWith("https://www.facebook.com/sharer/")) {
        return true;
    }
    if (status === -3) {
        return true;
    }

    return false;
}

/**
 *
 * @param errors {UrlCheckResponse[]}
 * @return {string}
 */
function createErrorList(errors) {
    return errors.map(err => `${createTabSpace(3)}${err.url}${createTabSpace()}${err.status}${err.message ? ` – ${err.message}` : ""}`).join("\n");
}

/**
 *
 * @param errors {UrlCheckResponse[]}
 * @return {string}
 */
function createErrorResult(errors) {
    let parentPages = "";
    let nestedPages = "";

    const parentPagesErrors = errors.filter(url => url.parentUrl === undefined);
    if (parentPagesErrors.length > 0) {
        parentPages = `${createTabSpace()}Pages from sitemap:\n${createErrorList(parentPagesErrors)}\n`;
    }

    const nestedPagesErrors = errors
        .filter(url => url.parentUrl !== undefined)
        .sort((prev, curr) => prev.parentUrl.localeCompare(curr.parentUrl));
    for (let i = 0; i < nestedPagesErrors.length; i++) {
        if (i === 0) {
            nestedPages = `${createTabSpace()}Nested pages:\n`;
            nestedPages += `${createTabSpace(1)}Page: ${nestedPagesErrors[i].parentUrl}\n`;
        } else {
            if (nestedPagesErrors[i].parentUrl === nestedPagesErrors[i - 1].parentUrl) {
                continue;
            } else {
                nestedPages += `${createTabSpace(1)}Page: ${nestedPagesErrors[i].parentUrl}\n`;
            }
        }
        const images = nestedPagesErrors.filter(err => err.parentUrl === nestedPagesErrors[i].parentUrl && err.isImg);
        const links = nestedPagesErrors.filter(err => err.parentUrl === nestedPagesErrors[i].parentUrl && !err.isImg);
        if (images.length > 0) {
            nestedPages += `${createTabSpace(2)}${IMAGES_LABEL}\n${createErrorList(images)}\n`;
        }
        if (links.length > 0) {
            nestedPages += `${createTabSpace(2)}${URLS_LABEL}\n${createErrorList(links)}\n`;
        }
    }

    return parentPages + nestedPages;
}

/**
 *
 * @param incorrectLinks {string[]}
 * @param webUrl {string}
 * @return {string[]}
 */
function createCorrectLinks(incorrectLinks, webUrl) {
    return [
        ...new Set(
            incorrectLinks
                .filter((i, url) => url && new RegExp("^(\\/|http)").test(url))
                .map((i, url) => (url.startsWith("http") ? url : webUrl + url)),
        ),
    ];
}

/**
 *
 * @param url {string}
 * @param parentUrl {string | undefined}
 * @param ttl {number}
 * @return {Promise<UrlCheckResponse>}
 */
async function fetchUrl(url, parentUrl = undefined, ttl = 1) {
    try {
        Sitemap.axios.defaults.maxRedirects = parentUrl ? 10 : 0;
        const encodedUrl = parentUrl ? encodeURI(decodeURI(url)) : url;

        const t0 = performance.now();
        const { status } = await Sitemap.axios.get(encodedUrl);
        const t1 = performance.now();

        if (status !== 200 && ttl < MAX_TTL) {
            return await fetchUrl(encodedUrl, parentUrl, ttl + 1);
        }

        return {
            url,
            parentUrl,
            isImg: isImageUrl(url),
            ttl,
            status,
            time: Math.ceil(t1 - t0),
        };
    } catch (e) {
        const status = Number.parseInt((e && e.response && e.response.status) || -1, 10);

        if (shouldIgnoreError(url, status, e)) {
            return {
                url,
                parentUrl,
                isImg: isImageUrl(url),
                ttl,
                status,
                shouldIgnoreError: true,
                time: 0,
            };
        }
        return {
            url,
            parentUrl,
            isImg: isImageUrl(url),
            ttl,
            status,
            time: 0,
            message: e.message,
            shouldIgnoreError: false,
        };
    }
}

/**
 *
 * @param url {string}
 * @param parentUrl {string | undefined}
 * @return {UrlCheckResponse}
 */
async function testUrl(url, parentUrl = undefined) {
    const indexInChecked = TESTED_URLS.findIndex(result => result.url === url);
    if (indexInChecked === -1) {
        const result = await fetchUrl(url, parentUrl);
        TESTED_URLS.push(result);
        return result;
    }
    return TESTED_URLS[indexInChecked];
}

/**
 *
 * @param urls {string[]}
 * @param webUrl {string}
 * @param sitemapUrl {string}
 * @param skip {number}
 * @param testNested {boolean}
 * @return {Promise<void>}
 */
async function testSitemapUrls(urls, webUrl, sitemapUrl, skip, testNested) {
    for (let i = skip || 0; i < urls.length; i++) {
        const url = urls[i];
        const changedUrl = webUrl ? `${webUrl}${new URL(url).pathname}` : null;

        printUrlInfo(changedUrl ?? url, i, urls.length);

        const result = await testUrl(changedUrl ?? url);
        printUrlResult(result);

        if (testNested && result.status === 200) {
            await testAllNestedUrls(changedUrl ?? url, i, webUrl ?? sitemapUrl.split("/").slice(0, 3).join("/"));
        }
    }
}

/**
 *
 * @param parentUrl {string}
 * @param parentIndex {number}
 * @param webUrl {string}
 * @return {Promise<void>}
 */
async function testAllNestedUrls(parentUrl, parentIndex, webUrl) {
    const { data } = await axios.get(parentUrl);
    const $ = cheerio.load(data);
    const urls = createCorrectLinks(
        $("a[href]").map((i, node) => $(node).attr("href")),
        webUrl,
    );
    const images = createCorrectLinks(
        $("img[src]").map((i, node) => $(node).attr("src")),
        webUrl,
    );

    await testNested(images, parentIndex, parentUrl, createTabSpace() + IMAGES_LABEL);
    await testNested(urls, parentIndex, parentUrl, createTabSpace() + URLS_LABEL);
}

/**
 *
 * @param urls {string[]}
 * @param parentIndex {number}
 * @param parentUrl {string}
 * @param label {string}
 * @return {Promise<void>}
 */
async function testNested(urls, parentIndex, parentUrl, label) {
    if (urls.length === 0) {
        return;
    }

    stdout.write(label + "\n");
    for (let i = 0; i < urls.length; i++) {
        if (TESTED_URLS.findIndex(result => result.url === urls[i]) !== -1) {
            continue;
        }
        printUrlInfo(urls[i], i, urls.length, `${createTabSpace(2)}(${parentIndex + 1}) `);
        const result = await testUrl(urls[i], parentUrl);
        printUrlResult(result);
    }
}

/**
 *
 * @param url {string}
 * @param urlIndex {number}
 * @param allUrlsCount {number}
 * @param prefix {string}
 */
function printUrlInfo(url, urlIndex, allUrlsCount, prefix = "") {
    stdout.write(`${prefix}${urlIndex + 1} / ${allUrlsCount}${createTabSpace()}${url}`);
}

/**
 *
 * @param result {UrlCheckResponse}
 */
function printUrlResult(result) {
    const { ttl, status, time, message } = result;
    stdout.write(`${createTabSpace()}${status}${message ? " – " + message : ""} (${time}ms) ttl=${ttl} ${status === 200 ? "✅ " : "❌ "}\n`);
}

/**
 *
 * @param errorText {string}
 * @param title {string}
 */
function logErrors(errorText, title) {
    stdout.write(title);
    stdout.write(`${errorText}\n\n`);
}

/**
 *
 * @param millis {number}
 * @return {string}
 */
function convertTime(millis) {
    let minutes = Math.floor(millis / 60000);
    let seconds = ((millis % 60000) / 1000).toFixed(0);
    return `${minutes} min ${seconds < 10 ? "0" : ""}${seconds} sec`;
}

/**
 *
 * @param okResults {UrlCheckResponse[]}
 * @param time {number}
 */
function logStatistics(okResults, time) {
    const avgTime = Math.round(okResults.reduce((prev, curr) => prev + curr.time, 0) / TESTED_URLS.length);
    const maxTime = okResults.reduce((prev, curr) => (curr.time > prev.time ? curr : prev), []);
    const minTime = okResults.reduce((prev, curr) => (curr.time < prev.time ? curr : prev), []);

    stdout.write("\nSummary:\n");
    stdout.write(createTabSpace() + `Time ${convertTime(time)}\n`);
    stdout.write(
        createTabSpace() + "Images tested:" + createTabSpace() + TESTED_URLS.filter(url => url.isImg).length + "\n",
    );
    stdout.write(
        createTabSpace() + "Links tested:" + createTabSpace() + TESTED_URLS.filter(url => !url.isImg).length + "\n",
    );
    stdout.write(createTabSpace() + "Avg time:" + createTabSpace() + avgTime + "ms\n");
    stdout.write(
        createTabSpace() + "Min time:" + createTabSpace() + minTime.time + "ms" + createTabSpace() + minTime.url + "\n",
    );
    stdout.write(
        createTabSpace() + "Max time" + createTabSpace() + maxTime.time + "ms" + createTabSpace() + maxTime.url + "\n",
    );
}

/**
 *
 * @param errorText {string}
 * @param slackChannel {string}
 * @return {Promise<void>}
 */
async function sendSlackMessage(errorText, slackChannel) {
    await Slack.chatPostMessage(slackChannel, {
        text: ":warning: Odkazy uvedené v sitemap.xml nejsou dostupné",
        attachments: [
            {
                text: errorText,
            },
        ],
    });
}

/**
 *
 * @param resultErrors {string}
 * @return {Promise<void>}
 */
async function sendGoogleChatMessage(resultErrors) {
    await GoogleChat.chatPostMessage({
        text: resultErrors
    });
}

/**
 *
 * @param sitemapUrl {string}
 * @param webUrl {string}
 * @param slackChannel {string}
 * @param skip {number}
 * @param testNested {boolean}
 * @return {Promise<*>}
 */
module.exports = async function run(sitemapUrl, webUrl, slackChannel, skip, testNested) {
    if (!sitemapUrl) {
        stdout.write("⛔ Required parameter --url is empty.\n");
        return process.exit(1);
    }

    if (slackChannel && !env.SLACK_TOKEN) {
        stdout.write("⛔ Environment variable SLACK_TOKEN is empty.\n");
        return process.exit(1);
    }

    if (webUrl) {
        stdout.write(`${createTabSpace()}Sitemap url: ${sitemapUrl}\n`);
        stdout.write(`❗${createTabSpace()}Web url is defined: ${webUrl}\n\n`);
    }

    const startTime = performance.now();
    await testSitemapUrls((await Sitemap.getSitemap(sitemapUrl)), webUrl, sitemapUrl, skip, testNested);
    const finishTime = performance.now();

    const errors = TESTED_URLS.filter(r => r.status !== 200 && r.shouldIgnoreError === false);
    const ignoredErrors = TESTED_URLS.filter(r => r.status !== 200 && r.shouldIgnoreError === true);
    const ok = TESTED_URLS.filter(r => r.status === 200);

    if (errors.length > 0) {
        const errorText = createErrorResult(errors);
        logErrors(errorText, "\nErrors:\n");
        const ignoredErrorText = createErrorResult(ignoredErrors);
        logErrors(ignoredErrorText, "\nIngored errors:\n");
        await sendSlackMessage(errorText, slackChannel);
        await sendGoogleChatMessage(errorText);
    }

    if (ignoredErrors.length > 0) {
        const ignoredErrorText = createErrorResult(ignoredErrors);
        logErrors(ignoredErrorText, "\nIngored errors:\n");
    }

    logStatistics(ok, Math.ceil(finishTime - startTime));

    process.exit(errors.length > 0 ? 1 : 0);
};
