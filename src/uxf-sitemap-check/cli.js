const { argv, env } = require("process");

module.exports = async () => {
    const cli = require("yargs")
        .command("$0", "UXF sitemap checker", yargs => {
            yargs.demandCommand(0, 0).usage(`UXF sitemap checker
Usage:
  uxf-sitemap-check [options]
  
Environment variables:
  HTTP_USERNAME - optional
  HTTP_PASSWORD - optional
  SLACK_TOKEN   - optional`);
        })
        .option("url", {
            describe: "Sitemap url",
            type: "string",
            group: "Options",
        })
        .option("web-url", {
            describe: "Web url for check",
            type: "string",
            group: "Options",
        })
        .option("slack-channel", {
            describe: "Slack channel id.",
            type: "string",
            group: "Options",
        })
        .option("u", {
            alias: "http-username",
            describe: "Http authorization username (or use environment variable HTTP_USERNAME)",
            type: "string",
            group: "Options",
        })
        .option("p", {
            alias: "http-password",
            describe: "Http authorization password (or use environment variable HTTP_PASSWORD)",
            type: "string",
            group: "Options",
        })
        .option("skip", {
            describe: "Number of skipped urls.",
            type: "string",
            group: "Options",
        })
        .option("test-nested", {
            describe: "If nested urls should be tested.",
            type: "boolean",
            group: "Options",
        })
        .option("h", { alias: "help", group: "Options" })
        .strict(false)
        .exitProcess(false);

    try {
        const { help, url, ...options } = cli.parse(argv.slice(2));
        const skip = options.skip ? Number.parseInt(options.skip) : null;
        const webUrl = options["web-url"] || null;

        if (Boolean(help)) {
            return 0;
        }

        if (options["http-username"] && options["http-password"]) {
            env.HTTP_USERNAME = options["http-username"];
            env.HTTP_PASSWORD = options["http-password"];
        }

        await require("./index")(url, webUrl, options["slack-channel"], skip, options["test-nested"]);
    } catch (e) {
        console.error(e);
        return 1;
    }
};
