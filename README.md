# @uxf/scripts

# @uxf/scripts
[![npm](https://img.shields.io/npm/v/@uxf/scripts)](https://www.npmjs.com/package/@uxf/scripts)
[![size](https://img.shields.io/bundlephobia/min/@uxf/scripts)](https://www.npmjs.com/package/@uxf/scripts)
[![quality](https://img.shields.io/npms-io/quality-score/@uxf/scripts)](https://www.npmjs.com/package/@uxf/scripts)
[![license](https://img.shields.io/npm/l/@uxf/scripts)](https://www.npmjs.com/package/@uxf/scripts)
## uxf-release
```
UXF release helper
Usage:
  uxf-release [options]

Environment variables:
  GITLAB_TOKEN      - required
  CI_SERVER_URL     - required - setting by GitLab CI
  CI_PROJECT_ID     - required - setting by GitLab CI
  SLACK_WEBHOOK_URL - optional

Options
  -p, --project-id  GitLab project id (or use environment variable
                    CI_PROJECT_ID)                                      [number]
  -d, --dry-run     Skip publishing and sending slack notification     [boolean]
  -h, --help        Show help                                          [boolean]

Options:
      --version  Show version number                                   [boolean]
```

### How to use in gitlab pipeline `.gitlab-ci.yml`

set environment variable `GITLAB_TOKEN` in GitLab CI configuration and create `.gitlab-ci.yml`

```yaml
stages:
  - release

release:
  image: node:12-buster-slim
  stage: release
  before_script:
    - yarn global add @uxf/release
  script:
    # ... deploy project ...
    - uxf-release
```
## uxf-lunch

```bash
UXF lunch
Usage:
  uxf-lunch [options]

Environment variables:
  SLACK_TOKEN - required

Options
      --slack-channel  Slack channel                                    [string]
  -h, --help           Show help                                       [boolean]

Options:
      --version  Show version number
```
