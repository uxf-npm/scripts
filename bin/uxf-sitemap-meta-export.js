#!/usr/bin/env node
require("../src/uxf-sitemap-meta-export/cli")()
    .then(exitCode => {
        process.exitCode = exitCode;
    })
    .catch(() => {
        process.exitCode = 1;
    });
