#!/usr/bin/env node
require("../src/uxf-lunch/cli")()
    .then(exitCode => {
        process.exitCode = exitCode;
    })
    .catch(() => {
        process.exitCode = 1;
    });
