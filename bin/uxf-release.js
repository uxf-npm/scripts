#!/usr/bin/env node
require("../src/uxf-release/cli")()
    .then(exitCode => {
        process.exitCode = exitCode;
    })
    .catch(() => {
        process.exitCode = 1;
    });
