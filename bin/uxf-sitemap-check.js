#!/usr/bin/env node
require("../src/uxf-sitemap-check/cli")()
    .then(exitCode => {
        process.exitCode = exitCode;
    })
    .catch(() => {
        process.exitCode = 1;
    });
